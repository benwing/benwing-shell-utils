#!/usr/bin/python

### check-same-files --- Look for files that are the same in the specified
### directories

## Copyright (C) 2007 Ben Wing.

## Author: Ben Wing <ben@benwing.com>
## Maintainer: Ben Wing <ben@benwing.com>
## Current Version: $Revision: 1.4 $

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED.  IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import optparse
import os, os.path
import traceback
import hashlib
import re

###########################################################################
#
# Command-line options and usage
#
###########################################################################

usage = """%prog [OPTIONS] DIRS ...

Given a list of directories, look among all the files in all the
directories for files that have the same content.  Files are considered
the same if they match in size and content (computed using an MD5 hash of
the content, so that the running time is linear in the number of files to
be processed)."""

opt_parser = optparse.OptionParser(usage=usage)
opt_parser.add_option("--debug", action="store_true",
                      help="Debugging output.")
opt_parser.add_option("-n", "--dry-run", action="store_true",
                      help="""Print the commands that would be executed, but
                      don't actually execute them.""")
opt_parser.add_option("-r", "--rename", action="store_true",
                      help="""Add info for use with renaming: The file is
                      doubled on the line with a recognizatory symbol @@@
                      between the two instances.""")
opt_parser.add_option("-s", "--size-only", action="store_true",
                      help="""Only compare the size when looking for matches.
                      That is, two files are considered the same if they have
                      the same size.  Normally, two files are considered the
                      same only if they have the same size and the MD5 hash of
                      their contents matches.  This option can potentially
                      improve performance on very large files, at the risk of
                      producing false positives.""")
opt_parser.add_option("-a", "--alpha", action="store_true",
                      help="""Just list all files, sorted alphabetically,
                      without duplicate checks.""")
opt_parser.add_option("--show-non-dups", "--snd", action="store_true",
                      help="""Show cases where two or more files have the
                      same size but not the same contents.""")
opt_parser.add_option("--prefer",
                      help="""When listing duplicates, if the files have the
                      same length, order them according to the list of trees
                      given.  If more than one directory is given, they should
                      be space-separated, and can be quoted (single or double)
                      if they contain spaces. FIXME: GIVE EXAMPLE""",
                      metavar="DIRS")
opt_parser.add_option("--delete",
                      help="""Delete duplicate files occurring inside the
                      specified directory trees.  If more than one directory
                      is given, they should be space-separated, and can be
                      quoted (single or double) if they contain spaces.
                      Whenever a set of duplicate files is processed, the
                      directories in this list are searched, in order, and
                      any files in those directories are deleted.  However,
                      deletion will not happen (instead, a warning is issued)
                      if all copies of a file would be deleted when processing
                      a single tree.  Essentially, the list of trees to
                      delete from specifies a preference list for determining
                      which copy to delete.  FIXME: GIVE EXAMPLE""",
                      metavar="DIRS")

# Example of how to handle a numeric argument
#opt_parser.add_option("-m", "--max_words", default=10000,
#                      help="Only score sentences with at most NUM words.",
#                      metavar="NUM")
#

(opts, args) = opt_parser.parse_args()

maxsize = 5000000

def split_argument(arg):
    '''Given an argument consisting of a string containing a
    space-separated list of values, possibly using quotes around embedded
    spaces, return a list of the values.'''
    # The arguments returned from re.findall() consist of tuples of the
    # capturing groups in the regex.  Because the groups are separated by |,
    # only one will be active.  Use reduce() to extract the one that's
    # active (non-empty).
    return [reduce(lambda a,b:a or b, x) for x in re.findall(r'''"((?:[^"]|\\")*)"|'((?:[^']|\\')*)'|(\S+)''', arg)]

def grab_all_files(args):
    '''Given a list of directories, recursively walk all files in all the
directories and create a hash table of files seen, indexed by their size.'''
    allfiles = {}
    for arg in args:
        for root, dirs, files in os.walk(arg, topdown=False):
            for name in files:
                file = os.path.join(root, name)
                try:
                    allfiles[file] = os.stat(file).st_size
                except:
                    # If error on stat, ignore it, but print it out
                    traceback.print_exc()
    return allfiles

def print_file(name):
    if opts.rename:
        print "  %s @@@ %s" % (name, name)
    else:
        print "  %s" % name

def is_prefix(a, b):
    # Return true if A is a path prefix of B.
    if a == b:
        return True
    elif not b:
        return False
    else:
        return is_prefix(a, os.path.split(b)[0])
        
def prefer_longer(prefer_list, a, b):
    (apath, aname) = os.path.split(a)
    (bpath, bname) = os.path.split(b)
    if len(aname) > len(bname):
        return 1
    elif len(bname) > len(aname):
        return -1
    else:
        for path in prefer_list:
            if is_prefix(path, apath):
                return 1
            elif is_prefix(path, bpath):
                return -1
        return 1

def sort_files_by_prefer_list(files, prefer_list):
    return sorted(files, cmp=lambda a,b:prefer_longer(prefer_list, a, b),
                  reverse=True)

def print_files_of_size(size, files):
    # Given the hash table of files indexed by size, print out the files
    # of a given size.
    print "---------------------------------------- Size %d (%d files):" % (size, len(files))
    # Sort files by decreasing length of file name (without the directory
    # part), in case (e.g.) we want to use the output to remove the
    # shortest-named file
    for y in files:
        print_file(y)

def compute_hash(file):
    # Compute the MD5 hash of a file and return in hex-digest form.
    m = hashlib.md5()
    fildesc = open(file, "rb")
    while True:
        block = fildesc.read(maxsize)
        if not block:
            break
        m.update(block)
    fildesc.close()
    return m.hexdigest()
    
def process_files_by_hash(size, files, delete_list, prefer_list):
    files_by_hash = {}
    for fil in files:
        dig = compute_hash(fil)
        files_by_hash[dig] = files_by_hash.get(dig, []) + [fil]
    for (hash, hashfiles) in files_by_hash.items():
        if len(hashfiles) == 1 and opts.show_non_dups:
            print "File not duplicated: hash: %s, file: %s" % (
                hash, hashfiles[0])
        if len(hashfiles) > 1:
            print_files_of_size(
                size, sort_files_by_prefer_list(hashfiles, prefer_list))
            if delete_list:
                # Delete non-preferred files over preferred ones
                files = reversed(sort_files_by_prefer_list(
                    hashfiles, prefer_list))
                files_to_delete = []
                for path in delete_list:
                    # WARNING! We need to precompute len(files) here because
                    # files is an iterator and its length changes (decreases)
                    # as you iterate over it.
                    lenfiles = len(files)
                    for x in files:
                        if x not in files_to_delete and is_prefix(path, x) \
                               and len(files_to_delete)+1 < lenfiles:
                            files_to_delete += [x]
                for x in files_to_delete:
                    print 'rm "%s"' % x
                    if not opts.dry_run:
                        os.remove(x)
        
def process_files_by_size(allfiles, prefer_list, process_fun):
    files_by_size = {}
    for (file, size) in allfiles.items():
        files_by_size[size] = files_by_size.get(size, []) + [file]
    for x in reversed(sorted(files_by_size)):
        files = files_by_size[x]
        if len(files) > 1:
            process_fun(x, files, prefer_list)

def process_and_handle_files_by_size(allfiles, delete_list, prefer_list):
    def handle_fun(x, files, prefer_list):
        if not opts.size_only:
            process_files_by_hash(x, files, delete_list, prefer_list)
        else:
            print_files_of_size(
                x, sort_files_by_prefer_list(files, prefer_list))
    process_files_by_size(allfiles, prefer_list, handle_fun)
   
def main():
    prefer_list = []
    delete_list = []
    if opts.prefer:
        prefer_list = split_argument(opts.prefer)
    if opts.delete:
        delete_list = split_argument(opts.delete)
    allfiles = grab_all_files(args)
    if opts.alpha:
        for x in sorted(allfiles):
            print_file(x)
    else:
        process_and_handle_files_by_size(allfiles, delete_list, prefer_list)

main()
