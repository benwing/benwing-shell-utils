#!/usr/bin/python

### apply-check-same-files --- Apply the results of changes made to
### check-same-files log files

## Copyright (C) 2008 Ben Wing.

## Author: Ben Wing <ben@benwing.com>
## Maintainer: Ben Wing <ben@benwing.com>
## Current Version: $Revision: 1.2 $

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED.  IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import optparse
import re
import os, os.path
import traceback

###########################################################################
#
# Command-line options and usage
#
###########################################################################

usage = """%prog [OPTIONS] DIRS ...

Given a file specifying a log file from check-same-files.py that has had
deletion and rename indications made to it, apply the deletions and renamings.
"""

opt_parser = optparse.OptionParser(usage=usage)
opt_parser.add_option("-d", "--debug", action="store_true",
                      help="Debugging output.")
opt_parser.add_option("-n", "--dry-run", action="store_true",
                      help="""Don't do anything; just print what would be done.""")

(opts, args) = opt_parser.parse_args()

assert len(args) > 0
def main():
    for line in open(args[0]):
        m = re.match(r"^d?  ([^ ].*) @@@ ([^ ].*)$", line)
        if m:
            if line[0] == 'd':
                doit('delete', m.group(2))
            elif m.group(1) != m.group(2):
                doit('rename', m.group(2), m.group(1))

def doit(action, file1, file2=None):
    if action == 'delete':
        print "Delete %s" % file1
        if not opts.dry_run:
            os.remove(file1)
    elif action == 'rename':
        print "Rename %s to %s" % (file1, file2)
        if not opts.dry_run:
            os.rename(file1, file2)
    else:
        assert False

main()
