: #-*- Perl -*-

### cvs-diff --- front end to `cvs diff'

## Copyright (C) 2001 Ben Wing.

## Author: Ben Wing <ben@benwing.com>
## Maintainer: Ben Wing <ben@benwing.com>
## Current Version: 1.1, April 6, 2001

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE CONTRIBUTORS ``AS IS'' AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED.  IN NO EVENT SHALL THE CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

eval 'exec perl -w -S $0 ${1+"$@"}' # Portability kludge
    if 0;

use strict;
use Getopt::Long;

# Extract out our own and all diff options so that we can reliably get
# the actual files to `cvs diff'.

sub GetDiffOptions {
  my $options_hash = shift;
  my @our_options = @_;

  $Getopt::Long::ignorecase = 0;
  &GetOptions (
	       $options_hash,
	       # our own options
	       @our_options,
	       # diff options
	       'l', 'R', 'D=s', 'N', 'r=s', 'ifdef=s',
	       'i', 'ignore-case',
	       'w', 'ignore-all-space',
	       'b', 'ignore-space-change',
	       'B', 'ignore-blank-lines',
	       'I=s', 'ignore-matching-lines=s',
	       'binary',
	       'a', 'text',
	       'c', 'C=i', 'context:i',
	       'u', 'U=i', 'unified:i',
	       '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
	       'L=s', 'label=s',
	       'p', 'show-c-function',
	       'F=s', 'show-function-line=s',
	       'q', 'brief',
	       'e', 'ed',
	       'n', 'rcs',
	       'y', 'side-by-side',
	       'W:i', 'width=i',
	       'left-column',
	       'suppress-common-lines',
	       # 'DNAME', 'ifdef=s', overridden by CVS
	       'old-group-format=s',
	       'new-group-format=s',
	       'unchanged-group-format=s',
	       'changed-group-format=s',
	       'line-format=s',
	       'old-line-format=s',
	       'new-line-format=s',
	       'unchanged-line-format=s',
	       # 'l', overridden by CVS
	       'paginate',
	       't', 'expand-tabs',
	       'T', 'initial-tab',
	       # 'r', overridden by CVS
	       'recursive',
	       # 'N', overridden by CVS
	       'new-file',
	       'P', 'unidirectional-new-file',
	       's', 'report-identical-files',
	       'x=s', 'exclude=s',
	       'X=s', 'exclude-from=s',
	       'S=s', 'starting-file=s',
	       'horizon-lines=i',
	       'd', 'minimal',
	       'H', 'speed-large-files',
	       'v', 'version',
	       'help',
	      );
}

1;
